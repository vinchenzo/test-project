import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {InterceptorService} from './interceptor.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {

  constructor(private http: HttpClient, private interceptorService: InterceptorService) {
  }

  public httpGet<T>(url: string, options: any = {}): Observable<any> {
    return this.http
      .get<T>(url, {params: options})
      .map((response) => response)
      .catch((error) => {
        return Observable.throw(this.interceptorService.handleError(error, url));
      });
  }
}

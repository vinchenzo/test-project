import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {FlightsModel} from '../models/flights.model';
import {Observable} from 'rxjs/Observable';
import {URL} from '../config/flights.url';
import {apiUrl} from '../config/common';
import {ResultModel} from '../models/result.model';
import * as moment from 'moment';


@Injectable()
export class SearchFlightsService {

  private getall = URL.getalldestination.url;
  private find = URL.findflight.url;

  constructor(private httpService: HttpService) {
  }

  public getFlights(): Observable<FlightsModel[]> {
    return this.httpService.httpGet<FlightsModel[]>(`${apiUrl + this.getall}`);
  }

  public findFlights(params): Observable<ResultModel[]> {
    return this.httpService.httpGet<ResultModel[]>(`${apiUrl + this.find + 'from/' + params.from + '/to/' + params.to + '/' +
    moment(params.go).format('YYYY-MM-DD') + '/' + moment(params.back).format('YYYY-MM-DD') +
    '/250/unique/?limit=15&offset-0'}`);
  }
}

export interface ResultModel {
  flights: [{
    currency: string;
    dateFrom: string;
    dateTo: string;
    price: number;
  }];
}

export interface FlightsModel {
  airports: [{
    base?: boolean;
    country: {
      code: string;
      currency: string;
      englishSeoName: string;
      name: string;
      seoName: string;
      url: string;
    }
    iataCode?: string;
    latitude: number;
    longitude: number;
    name: string;
  }];
  countries: [{
    code?: string;
    currency: string;
    englishSeoName: string;
    name?: string;
    seoName: string;
    url?: string;
  }];
  description: string;
  id: number;
  routes: object;
}

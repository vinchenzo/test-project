import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {CustomInputComponent} from './custom-input.component';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../material/material.module';
import {FilterPipe} from '../../pipe/filter.pipe';
import {ArraySortPipe} from '../../pipe/sort.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    CustomInputComponent,
    FilterPipe,
    ArraySortPipe
  ],
  exports: [
    CustomInputComponent
  ],
  providers: [],
  bootstrap: []
})

export class CustomInputModuleModule {

}

import {Component, OnInit, Input, OnDestroy, Output, EventEmitter, ElementRef} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {SearchFlightsService} from '../../services/search-flights.service';

@Component({
  selector: 'custom-input',
  styleUrls: ['./custom-input.component.scss'],
  templateUrl: './custom-input.component.html',
  host: {
    '(document:click)': 'onClick($event)',
  },
})

export class CustomInputComponent implements OnInit, OnDestroy {
  public visible = false;
  public filteredcountry = [];
  public filteredairport = [];
  public filteredCountries = [];
  public filteredAirports = [];
  public search = this.fb.group({
    'destination': new FormControl('', Validators.required),
  });
  private displayData: any;
  @Input() placeholder: string;
  @Input() from: string;
  @Input() patchCode: string;

  @Output()
  change: EventEmitter<object> = new EventEmitter<object>();

  constructor(private fb: FormBuilder,
              private getData: SearchFlightsService,
              private _eref: ElementRef) {
  }

  public ngOnInit() {
    this.getData.getFlights().subscribe((message) => {
      this.displayData = message;
      if (this.patchCode) {
        this.findAirport(this.patchCode);
      }
    });
    this.search.valueChanges.subscribe((value) => {
      this.autoCompleat(value.destination);
    });
  }

  public ngOnDestroy() {
    this.visible = false;
  }

  public openDropDown(value) {
    this.filteredcountry = this.displayData.countries;
    this.filteredairport = this.displayData.airports;
    if (this.from) {
      this.serchDestination(this.from);
    }
    this.visible = value;
  }

  private autoCompleat(key: string) {
    if (key) {
      this.filteredCountries = this.filteredcountry.filter((value) => {
        return value.englishSeoName.startsWith(key.toLowerCase());
      });
      this.filteredAirports = this.filteredairport.filter((value) => {
        return value.name.toUpperCase().startsWith(key.toUpperCase()) || value.country.name.toUpperCase().startsWith(key.toUpperCase());
      });
    } else {
      this.filteredCountries = [];
      this.filteredAirports = [];
    }
  }

  public setActive(key) {
    const setactiveCountry = this.filteredCountries.filter((value) => {
      return value.name.startsWith(key);
    });
    const setactiveAirport = this.filteredAirports.filter((value) => {
      return value.country.name.startsWith(key);
    });
    return setactiveCountry.length || setactiveAirport.length;
  }

  public returnItem(item) {
    this.search.patchValue({['destination']: item.name});
    this.change.emit({dest: this.placeholder.toLowerCase(), value: item});
    this.openDropDown(false);
  }

  private serchDestination(code) {
    this.filteredcountry = [];
    this.filteredairport = [];
    const map = new Map();
    const obj = this.displayData.routes;
    Object.keys(obj).forEach(key => {
      map.set(key, obj[key]);
    });
    const searched = map.get(code);
    const countries = searched.map(element => {
      return this.displayData.airports.filter((elem) => {
        return elem.iataCode === element;
      });
    });
    countries.forEach(element => {
      element.forEach((entries) => {
        const filt = this.filteredcountry.filter((elem) => {
          return elem.name === entries.country.name;
        });
        if (!filt.length) {
          this.filteredcountry.push(entries.country);
        }
        this.filteredairport.push(entries);
      });
    });
  }

  private findAirport(code) {
    const patchairport = this.displayData.airports.find((elem) => {
      return elem.iataCode === code;
    });
    this.search.patchValue({['destination']: patchairport.name});
  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) {
      this.openDropDown(false);
    }
  }
}


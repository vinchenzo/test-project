import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ROUTES } from './app.routes';

import { AppComponent } from './app.component';
import {RouterModule} from '@angular/router';
import {CoreModule} from './module/core/core.module';
import {NoContentComponent} from './no-content';
import {HttpService} from './shared/services/http.service';
import {InterceptorService} from './shared/services/interceptor.service';
import {SearchFlightsService} from './shared/services/search-flights.service';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    NoContentComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES, {
      useHash: true
    }),
    CoreModule,
    HttpClientModule
  ],
  providers: [
    HttpService,
    InterceptorService,
    SearchFlightsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

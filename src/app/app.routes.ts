import { Routes } from '@angular/router';
import { NoContentComponent } from './no-content';
import { coreRoutes } from './module/core/core.routing';

export const ROUTES: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  ...coreRoutes,
  {path: '**', component: NoContentComponent}
];

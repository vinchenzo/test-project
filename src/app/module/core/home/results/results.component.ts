import {Component, OnInit} from '@angular/core';
import {SearchFlightsService} from '../../../../shared/services/search-flights.service';
import {ActivatedRoute} from '@angular/router';
import {ResultModel} from '../../../../shared/models/result.model';

@Component({
  selector: 'results',
  styleUrls: ['./results.component.scss'],
  templateUrl: './results.component.html'
})
export class ResultsComponent implements OnInit {
  public results = [];

  constructor(private findFlight: SearchFlightsService,
              private activatedRoute: ActivatedRoute) {
  }

  public ngOnInit() {
    this.activatedRoute.queryParams
      .subscribe((params) => {
        this.findFlight.findFlights(params).subscribe((message) => {
          this.results = message;
        });
      });
  }
}

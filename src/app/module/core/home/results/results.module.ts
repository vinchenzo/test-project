import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {ResultsComponent} from './results.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [],
  exports: [],
  providers: [],
  entryComponents: [ResultsComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class ResultsModule {
}

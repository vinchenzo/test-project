import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home.component';
import {ResultsComponent} from './results/results.component';

export const homeRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    children: [
      {
        path: 'result',
        component: ResultsComponent,
      }
    ]
  }
];

export const appRoutingProviders: any[] = [];
export const systemAdministrationRouting: ModuleWithProviders = RouterModule.forChild(homeRoutes);

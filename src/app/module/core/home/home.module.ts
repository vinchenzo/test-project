import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home.component';
import {MaterialModule} from '../../../shared/module/material/material.module';
import {SearchFormComponent} from './search-form/search-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CustomInputModuleModule} from '../../../shared/module/custom-input/custom-input.module';
import {ResultsComponent} from './results/results.component';
import {ResultsModule} from './results/results.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    CustomInputModuleModule,
    ResultsModule
  ],
  declarations: [
    HomeComponent,
    SearchFormComponent,
    ResultsComponent
  ],
  exports: [
    HomeComponent,
    SearchFormComponent,
  ],
  providers: [],
  entryComponents: [SearchFormComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class HomeComponentModule {
}

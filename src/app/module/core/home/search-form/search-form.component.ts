import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import {FlightsModel} from '../../../../shared/models/flights.model';
import * as moment from 'moment';
import {ActivatedRoute, Route, Router} from '@angular/router';

@Component({
  selector: 'search-form',
  styleUrls: ['./search-form.component.scss'],
  templateUrl: './search-form.component.html'
})
export class SearchFormComponent implements OnInit {
  public allflights: FlightsModel[];
  public godate;
  public haveparams: boolean;
  public backdate;
  public search = this.fb.group({
    'from': new FormControl('', Validators.required),
    'to': new FormControl('', Validators.required),
    'go': new FormControl('', Validators.required),
    'back': new FormControl('', Validators.required),
  });

  constructor(private fb: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  public ngOnInit() {
    this.activatedRoute.queryParams
      .subscribe((params) => {
        this.haveparams = !!params.from;
        this.search.patchValue(params);
        this.godate = new Date(params.go);
        this.backdate = new Date(params.back);
      });
  }

  public elementSet(event) {
    this.search.patchValue({[event.dest]: event.value.iataCode});
  }

  public dateSet(prop, event) {
    this.search.patchValue({[prop]: moment(event).format('YYYY-MM-DD')});
  }

  public goSearch() {
    this.router.navigate(['./result'], {queryParams: this.search.value, relativeTo: this.activatedRoute});
  }
}

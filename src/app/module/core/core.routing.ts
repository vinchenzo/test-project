import {ModuleWithProviders} from '@angular/core';
import {CoreComponent} from './core.component';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {SearchFormComponent} from './home/search-form/search-form.component';
import {homeRoutes} from './home/home.routing';

export const coreRoutes: Routes = [{
  path: '',
  component: CoreComponent,
  children: [
    ...homeRoutes
  ]
}];

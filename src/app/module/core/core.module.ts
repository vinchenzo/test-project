import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {CoreComponent} from './core.component';
import {HomeComponentModule} from './home/home.module';
import {MaterialModule} from '../../shared/module/material/material.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HomeComponentModule,
    MaterialModule,
  ],
  exports: [],
  declarations: [CoreComponent],
  providers: []
})
export class CoreModule {
}

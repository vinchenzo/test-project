import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'test-core',
  styleUrls: ['./core.component.scss'],
  templateUrl: './core.component.html',
})

export class CoreComponent implements OnInit {

  public modeMenuSmall: boolean = false;
  public state = 'default';
  public options = {minScrollbarLength: 120, maxScrollbarLength: 120};

  constructor() {

  }

  public ngOnInit() {
  }
}
